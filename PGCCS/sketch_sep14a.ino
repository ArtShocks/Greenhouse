//Programmable greenhouse climate control system

#include <TroykaDHT.h> // Library for working with the DHT22 sensor
#include <QuadDisplay2.h> // Library to work with the display
#define RELAY_1_PIN  A4 //Setting the pin under the bind for the relay one
#define RELAY_2_PIN  A2 // The same , but under the second

DHT dht(4, DHT22);
 
QuadDisplay qd(9); /* Create an object of the DHT22 class and 
pass the pin number to which the sensor is connected */

bool state = true; /* A variable for storing the state of the system. 
Responsible for what to display: temperature or humidity */

float temperature = 0; // Variable for storing the ambient temperature indicator

float humidity = 0; // Variable for storing the ambient humidity indicator
 
long currentMillis = 0;

void setup()
{
  
Serial.begin(9600); // Opening a serial port for monitoring actions in the program
  qd.begin(); // getting started with the DHT22 sensor
  dht.begin(); // assign 2 pins in exit mode
  pinMode(RELAY_1_PIN, OUTPUT);
  pinMode(RELAY_2_PIN, OUTPUT);
  currentMillis = millis();
}
 
void loop()
{
  if (millis() - currentMillis > 3000) {
    state = !state;
    currentMillis = millis();
    dht.read();
  }
// If the specified time interval has passed, the sensor readings are read
  switch (dht.getState()) {
// everything is very
    case DHT_OK:
// Temperature and humidity readings are read from the sensor
      temperature = dht.getTemperatureC();
      humidity = dht.getHumidity();
// Displays temperature or humidity readings. Switching results is possible with a button.
      if (state) {
        qd.displayTemperatureC(temperature);
      } else {
        qd.displayHumidity(humidity);
      }
      break;
// If there is an error, we display "Err"
    default:
      qd.displayDigits(QD_NONE, QD_E, QD_r, QD_r);
      break;
  }
// If the temperature exceeds 33 degrees
  if (temperature > 33) {
    // включаем реле 1
    digitalWrite(RELAY_1_PIN, HIGH);
  } else {
// Otherwise turn off the relay
    digitalWrite(RELAY_1_PIN, LOW);
  }
// If the humidity has become less than 44 %
  if (humidity < 44) {
// Turn on relay 2
    digitalWrite(RELAY_2_PIN, HIGH);
  } else {
// Otherwise turn off relay 2
    digitalWrite(RELAY_2_PIN, LOW);
  }
// Waiting for 0,75 second
  delay(750);
}

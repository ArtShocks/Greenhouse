# Greenhouse

All to the code for the arduino uno board. The purpose of the code is to create control over the microclimate in greenhouses, taking into account remote control and adjustment of modes optimal for certain succulents.



Project_name: Greenhouse.

Description: This project is designed to create a code for the Arduino Uno programmable board for monitoring the microclimate in a block greenhouse, by reading information from sensors directly connected to the programmable board and remotely controlled with its help. This code is designed to solve this problem.

Technology used in project: Arduino IDE.

Feature of the project:

In the project, for programming specific conditions for a specific succulent, a cross-cutting classification of plants is provided.

Phenological End-to-End Classification of Plants (FGC) The purpose of this classification is the ordering of plants based on the principles set out below.

Principles:

    In this classification, plants are coded according to their morphology and characteristics of the plant as a whole.

    Plants within this classification are designated in the form of writing a sentence consisting of: Latin letters; a slash, a comma, a colon and a dot as a syntax that allows decoding the sentence into a morphological description of the plant in the tillering phase.

    Parts of plants to distinguish them from their characteristics are indicated by capital letters, while the characteristic of a particular feature of any plant organ will be indicated by three lowercase letters of the Latin alphabet.

    Finding a plant by morphological characteristics, regardless of which taxon it belongs to.

    The classification is designed to highlight the most atypical and non-characteristic features of the total mass of flora by highlighting them as well as parts of the plant - in three capital letters.

Accepted abbreviations:

Root System - Root System (RS)

Mochkovataya - fibrous root system (Frs) Rod - rod system of roots (Rsr) Rhizome - rhizome(RH) Mixed root system - mixed root system(Mrs) Mycorrhiza (fungal root) - fungal root (FR) Nodules - nodules(N)

Escape - Sprout (SP)

Branch type of shoot (Bth) Dichotomous - dichotomus (dic) Monopodial - monipodial (mon) Sympodial - sympodial (sym) False dichotomous - false dichotomus (false dic)

Leaf arrangement - leaf arrangement (Lea)

Opposite - opposite(opp) Whorled - whoreled(who) The next one is regular(reg) Rosette - rossette(ross)

Leaf/Foliage - Foliage (FOL)

Simple - simple(Simp)

Triple - triple(tri) Feathery - feathery(fea) Fingered - palmate(pal) Paired - pair(pai) Unpaired- unpaid(unp)

Difficult - difficult (Diff)

(By analogy to do)

Upper - Top (Top) Lower - lower (Low) Basal - radical (Rad)

With stipules - with stipule (ws) Without stipules -without stipules (wts) With a leafy vagina - with a leafy vagina (wlv) Without a leafy vagina - without leaf sheath (wls)

The edge of the sheet plate - the edge of the plate (Teotp)

Whole-extreme - whole-extreme (w-ex) Serrated - serrated (srrd) Sawtooth - serrate (srre) Gorodchaty - crenate (cre) Notched - emarginate (ema)

Sheet form - form of foliage (Fof)

Needle - needle (nee) Cordate - cordate (cor) Oval - oval(ova) Rounded - rounded(rou)

Types of venation - types of renation tor (Tor)

Fingered - palmate(pal) Arc - arc(arc) Parallel - parallel(par) Reticular - reticular(ret)

Petiolar - petiolar (PET) Sedentary - sedentary (SED)

Example of a description of a plant - Aloe arborescens Mill.

#Visual display in classification (Aloe arborescens Mill.):

RSFrs/SPBthmon/Leaross/FolSimppaiRad/Teotpsrrd/Fofnee/Torpar/SED
